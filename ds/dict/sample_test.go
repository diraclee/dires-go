package dict

import (
	"github.com/DiracLee/dires-go/logger"
	"github.com/magiconair/properties/assert"
	"testing"
)

func TestNewSample(t *testing.T) {
	_ = NewSample()
}

func TestSampleDict_Len(t *testing.T) {
	dict := NewSample()
	assert.Equal(t, dict.Len(), 0)
}

func TestSampleDict_PutOrSet(t *testing.T) {
	dict := NewSample()
	assert.Equal(t, dict.PutOrSet("one", 1), 1)
	assert.Equal(t, dict.PutOrSet("one", "1"), 0)
	val, exists := dict.Get("one")
	assert.Equal(t, val, "1")
	assert.Equal(t, exists, true)
}

func TestSampleDict_PutIfNotExists(t *testing.T) {
	dict := NewSample()
	assert.Equal(t, dict.PutIfNotExists("one", 1), 1)
	assert.Equal(t, dict.PutIfNotExists("one", "1"), 0)
	val, exists := dict.Get("one")
	assert.Equal(t, val, 1)
	assert.Equal(t, exists, true)
}

func TestSampleDict_SetIfExists(t *testing.T) {
	dict := NewSample()
	assert.Equal(t, dict.SetIfExists("one", 1), 0)
	assert.Equal(t, dict.SetIfExists("one", "1"), 0)
	val, exists := dict.Get("one")
	assert.Equal(t, val, nil)
	assert.Equal(t, exists, false)
}

func TestSampleDict_Remove(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.Remove("one")

	val, exists := dict.Get("one")
	assert.Equal(t, val, nil)
	assert.Equal(t, exists, false)

	val, exists = dict.Get("two")
	assert.Equal(t, val, 2)
	assert.Equal(t, exists, true)
}

func TestSampleDict_ForEach(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.PutOrSet("three", 3)

	dict.ForEach(func(key string, val interface{}) bool {
		logger.Debugf("%v: %v", key, val)
		return true
	})
}

func TestSampleDict_Keys(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.PutOrSet("three", 3)

	keys := dict.Keys()
	logger.Debug(keys)
}

func TestSampleDict_RandomKeys(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.PutOrSet("three", 3)

	keys := dict.RandomKeys(2)
	logger.Debug(keys)
}

func TestSampleDict_RandomDistinctKeys(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.PutOrSet("three", 3)

	keys := dict.RandomDistinctKeys(2)
	logger.Debug(keys)
}

func TestSampleDict_Clear(t *testing.T) {
	dict := NewSample()
	dict.PutOrSet("one", 1)
	dict.PutOrSet("two", 2)
	dict.PutOrSet("three", 3)

	dict.Clear()
	assert.Equal(t, dict.Len(), 0)
}
