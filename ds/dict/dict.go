package dict

type Consumer func(key string, val interface{}) bool

type Dict interface {
	Len() int
	Get(key string) (val interface{}, exists bool)
	PutOrSet(key string, val interface{}) (result int)
	PutIfNotExists(key string, val interface{}) (result int)
	SetIfExists(key string, val interface{}) (result int)
	Remove(key ...string) (result int)
	ForEach(consumer Consumer) bool
	Keys() []string
	RandomKeys(limit int) []string
	RandomDistinctKeys(limit int) []string
	Clear()
}
