package set

type Set interface {
	naiveSet
	Intersect(another Set) Set
	Union(another Set) Set
	Diff(another Set) Set
}
