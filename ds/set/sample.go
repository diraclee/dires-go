package set

import "github.com/DiracLee/dires-go/ds/dict"

type sampleSet struct {
	naiveSet
}

// NewSample returns a sample naiveSetImpl.
func NewSample(vals ...string) Set {
	set := &sampleSet{newNaiveSet(dict.NewSample())}
	for _, val := range vals {
		set.Add(val)
	}
	return set
}

// Intersect returns a naiveSetImpl that contains elements which both two sets has.
func (s *sampleSet) Intersect(another Set) Set {
	result := NewSample()
	s.ForEach(func(elem string) bool {
		if another.Has(elem) {
			result.Add(elem)
		}
		return true
	})
	return result
}

// Union returns a naiveSetImpl that contains elements whichever of the two sets has.
func (s *sampleSet) Union(another Set) Set {
	result := NewSample()
	s.ForEach(func(elem string) bool {
		result.Add(elem)
		return true
	})
	another.ForEach(func(elem string) bool {
		result.Add(elem)
		return true
	})
	return result
}

// Diff returns a naiveSetImpl that contains elements which the former has and the latter does not have.
func (s *sampleSet) Diff(another Set) Set {
	result := NewSample()
	s.ForEach(func(elem string) bool {
		if !another.Has(elem) {
			result.Add(elem)
		}
		return true
	})
	return result
}
