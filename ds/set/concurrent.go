package set

import "github.com/DiracLee/dires-go/ds/dict"

type concurrentSet struct {
	naiveSet
}

// NewConcurrent returns a syncx naiveSetImpl.
func NewConcurrent(tableSize uint32) Set {
	return &concurrentSet{newNaiveSet(dict.NewSample())}
}

// Intersect returns a naiveSetImpl that contains elements which both two sets has.
func (s *concurrentSet) Intersect(another Set) Set {
	result := NewConcurrent(0)
	s.ForEach(func(elem string) bool {
		if another.Has(elem) {
			result.Add(elem)
		}
		return true
	})
	return result
}

// Union returns a naiveSetImpl that contains elements whichever of the two sets has.
func (s *concurrentSet) Union(another Set) Set {
	result := NewConcurrent(0)
	s.ForEach(func(elem string) bool {
		result.Add(elem)
		return true
	})
	another.ForEach(func(elem string) bool {
		result.Add(elem)
		return true
	})
	return result
}

// Diff returns a naiveSetImpl that contains elements which the former has and the latter does not have.
func (s *concurrentSet) Diff(another Set) Set {
	result := NewConcurrent(0)
	s.ForEach(func(elem string) bool {
		if !another.Has(elem) {
			result.Add(elem)
		}
		return true
	})
	return result
}
