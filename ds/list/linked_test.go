package list

import (
	"github.com/DiracLee/dires-go/logger"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNew(t *testing.T) {
	_ = New(1, 2, 3, 4, 5, 6, 7)
}

func TestLinkedList_Len(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	logger.Debug(l)
	assert.Equal(t, l.Len(), int64(7))
}

func TestLinkedList_Append(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	l.Append(8)
	logger.Debug(l)
	n, err := l.Get(7)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(8))
	assert.Equal(t, l.Len(), int64(8))
}

func TestLinkedList_Get(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	logger.Debug(l)
	n, err := l.Get(0)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(1))
	assert.Equal(t, l.Len(), int64(7))
}

func TestLinkedList_Set(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	err := l.Set(0, 10)
	assert.Equal(t, err, nil)
	logger.Debug(l)
	n, err := l.Get(0)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(10))
	assert.Equal(t, l.Len(), int64(7))
}

func TestLinkedList_Insert(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	err := l.Insert(0, 10)
	assert.Equal(t, err, nil)
	logger.Debug(l)
	n, err := l.Get(0)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(10))
	n, err = l.Get(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(1))
	assert.Equal(t, l.Len(), int64(8))
}

func TestLinkedList_Remove(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	val, err := l.Remove(3)
	assert.Equal(t, err, nil)
	assert.Equal(t, val, interface{}(4))
	logger.Debug(l)
	n, err := l.Get(3)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(5))
	n, err = l.Get(4)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(6))
	assert.Equal(t, l.Len(), int64(6))
}

func TestLinkedList_RemoveLast(t *testing.T) {
	l := New(1, 2, 3, 4, 5, 6, 7)
	l.RemoveLast()
	logger.Debug(l)
	assert.Equal(t, l.Len(), int64(6))
}

func TestLinkedList_RemoveValued_NonCount(t *testing.T) {
	l := New(1, 2, 2, 4, 5, 6, 7)
	l.RemoveValued(2)
	logger.Debug(l)
	n, err := l.Get(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(4))
	assert.Equal(t, l.Len(), int64(5))
}

func TestLinkedList_RemoveValued_WithCount(t *testing.T) {
	l := New(1, 2, 2, 5, 2, 6, 7)
	l.RemoveValued(2, 2)
	logger.Debug(l)
	n, err := l.Get(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(5))
	assert.Equal(t, l.Len(), int64(5))
}

func TestLinkedList_ReverseRemoveValued_NonCount(t *testing.T) {
	l := New(1, 2, 2, 4, 2, 6, 7)
	l.RemoveValued(2)
	logger.Debug(l)
	n, err := l.Get(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(4))
	assert.Equal(t, l.Len(), int64(4))
}

func TestLinkedList_ReverseRemoveValued_WithCount(t *testing.T) {
	l := New(1, 2, 2, 4, 2, 6, 7)
	l.ReverseRemoveValued(2, 2)
	logger.Debug(l)
	n, err := l.Get(1)
	assert.Equal(t, err, nil)
	assert.Equal(t, n, interface{}(2))
	assert.Equal(t, l.Len(), int64(5))
}

func TestLinkedList_Contains(t *testing.T) {
	l := New(1, 2, 2, 4, 2, 6, 7)
	logger.Debug(l)
	assert.Equal(t, l.Contains(2), true)
	assert.Equal(t, l.Contains(8), false)
}

func TestLinkedList_Range(t *testing.T) {
	l := New(1, 2, 2, 4, 2, 6, 7)
	s := l.Range(2, 4)
	for i, v := range s {
		n, err := l.Get(int64(i + 2))
		assert.Equal(t, err, nil)
		assert.Equal(t, v, n)
	}
}

