package list

type Consumer func(int, interface{}) bool

type List interface {
	Len() int
	Get(index int) (interface{}, error)
	Set(index int, val interface{}) error
	Append(val interface{})
	Insert(index int, val interface{}) error
	Remove(index int) (interface{}, error)
	RemoveLast() interface{}
	RemoveValued(val interface{}, count ...int) int
	ReverseRemoveValued(val interface{}, count ...int) int
	ForEach(consumer Consumer) bool
	Contains(val interface{}) bool
	Range(start, end int) []interface{}
	String() string
}

func New(values ...interface{}) List {
	l := newEmptyLinkedList()
	for _, val := range values {
		l.Append(val)
	}
	return l
}
