package list

import (
	"github.com/DiracLee/dires-go/logger"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	logger.initLogger()
	os.Exit(m.Run())
}
