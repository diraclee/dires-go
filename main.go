package main

import (
	"context"
	"github.com/DiracLee/dires-go/app/config"
	"github.com/DiracLee/dires-go/logger"
	"github.com/DiracLee/dires-go/tcp"
	"github.com/DiracLee/dires-go/tcp/handler"
	"github.com/fatih/color"
)

const (
	banner = `
   _______   __
   \   __  \|__/_ ___   ___    ____
   /  / /  /  \  ' _ \/ __  \/  ___/
  /  /_/  //  /  /   /  ___ /\___ \
/_______ /|__/__/    \ ___ /____ /
`
)

func main() {
	printBanner()
	ctx := context.Background()
	err := tcp.ListenAndServerUntilExitSignal(
		ctx,
		tcp.NewConfig(
			tcp.WithAddress(config.GetAddress()),
			tcp.WithMaxConnect(config.GetMaxConnect()),
			tcp.WithTimeout(config.GetTimeout()),
		),
		handler.NewDiresHandler(),
	)
	if err != nil {
		logger.Error(err)
	}
}

func printBanner() {
	_, _ = color.New(color.FgHiGreen, color.Bold).Println(banner)
}
