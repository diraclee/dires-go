package cluster

import (
	"github.com/DiracLee/dires-go/app/cmdline"
	"github.com/DiracLee/dires-go/app/connection"
	"github.com/DiracLee/dires-go/app/payload"
	"github.com/DiracLee/dires-go/app/server"
	"github.com/DiracLee/dires-go/logger"
)

type ClusterNode struct {
}

func NewClusterNode() server.Server {
	return &ClusterNode{}
}


func (svr *ClusterNode) Execute(conn connection.Connection, cmdLine cmdline.CmdLine) payload.Payload {
	logger.Infof("execute command line: %s", cmdLine)
	return payload.NewOkPayload()
}

func (svr *ClusterNode) Disconnect(conn connection.Connection) {
	//TODO implement me
}

func (svr *ClusterNode) Close() {
	//TODO implement me
}
