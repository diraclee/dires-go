package server

import (
	"github.com/DiracLee/dires-go/app/cmdline"
	"github.com/DiracLee/dires-go/app/connection"
	"github.com/DiracLee/dires-go/app/payload"
)


type Server interface {
	Execute(conn connection.Connection, cmdLine cmdline.CmdLine) payload.Payload
	Disconnect(conn connection.Connection)
	Close()
}
