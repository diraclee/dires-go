package database

import (
	"github.com/DiracLee/dires-go/ds/dict"
	"github.com/DiracLee/dires-go/syncx"
)

// DB TODO: database.Storage as the command group keys
type DB interface {
	Storage
	SetProxy
	DictProxy
	ListProxy
	SortedSetProxy
	StringProxy
}

func New() DB {
	return &diresDB{
		data:        dict.NewConcurrent(dataCount),
		keysTTL:     dict.NewConcurrent(ttlCount),
		keysVersion: dict.NewConcurrent(dataCount),
		LockBucket:  syncx.NewLockBucket(lockBucketSize),
		addAOF: func(cmdLine [][]byte) {},
	}
}

func NewSample() DB {
	return &diresDB{
		data:        dict.NewSample(),
		keysTTL:     dict.NewSample(),
		keysVersion: dict.NewSample(),
		LockBucket:  syncx.NewLockBucket(1),
		addAOF: func(cmdLine [][]byte) {},
	}
}
