package database

import (
	"github.com/DiracLee/dires-go/app/payload"
	"github.com/DiracLee/dires-go/ds/dict"
	"github.com/DiracLee/dires-go/ds/list"
	"github.com/DiracLee/dires-go/ds/set"
	"github.com/DiracLee/dires-go/ds/sortedset"
)

type SetProxy interface {
	GetAsSet(key string) (set.Set, payload.ErrorPayload)
	GetOrInitSet(key string) (set set.Set, inited bool, errPayload payload.ErrorPayload)
}

type DictProxy interface {
	GetAsDict(key string) (dict.Dict, payload.ErrorPayload)
	GetOrInitDict(key string) (dict dict.Dict, inited bool, errPayload payload.ErrorPayload)
}

type ListProxy interface {
	GetAsList(key string) (list.List, payload.ErrorPayload)
	GetOrInitList(key string) (list list.List, isNew bool, errPayload payload.ErrorPayload)
}

type SortedSetProxy interface {
	GetAsSortedSet(key string) (*sortedset.SortedSet, payload.ErrorPayload)
	GetOrInitSortedSet(key string) (sortedSet *sortedset.SortedSet, inited bool, errPayload payload.ErrorPayload)
}

type StringProxy interface {
	GetAsString(key string) ([]byte, payload.ErrorPayload)
}