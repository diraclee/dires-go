package database

import (
	"github.com/DiracLee/dires-go/app/payload"
	"github.com/DiracLee/dires-go/syncx"
	"time"
)

const (
	dataCount      = 1 << 16
	ttlCount       = 1 << 10
	lockBucketSize = 1 << 10
)

type DataEntity struct {
	Data interface{}
}

func NewDataEntity(data interface{}) *DataEntity {
	return &DataEntity{Data: data}
}

type Storage interface {
	SetIndex(index int)
	GetIndex() int

	SetAddAOF(func([][]byte))
	AddAOF([][]byte)

	Get(key string) (*DataEntity, bool)
	PutOrSet(key string, entity *DataEntity) int
	SetIfExists(key string, entity *DataEntity) int
	PutIfNotExists(key string, entity *DataEntity) int

	SetKeyTTL(key string, ttl time.Time)
	GetTTL(key string) (time.Time, bool)

	Delete(keys ...string) int

	Flush()

	syncx.LockBucket

	Persist(key string)
	IsExpired(key string) bool

	GetVersion(key string) int64
	AddVersion(keys ...string)

	ForEach(func(key string, val interface{}) bool)
	ForEachWithExpire(cb func(key string, data *DataEntity, expiration *time.Time) bool)

	ExecWithLock(cmdLine [][]byte) payload.Payload
}
