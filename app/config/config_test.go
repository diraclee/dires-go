package config

import (
	"github.com/DiracLee/dires-go/logger"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestGetAddress(t *testing.T) {
	logger.Debugf(GetAddress())
}

func TestGetMaxConnect(t *testing.T) {
	logger.Debug(GetMaxConnect())
}

func TestGetTimeout(t *testing.T) {
	logger.Debug(GetTimeout())
}

func TestGetAppendOnly(t *testing.T) {
	logger.Debug(GetAppendOnly())
}

func TestGetAppendFile(t *testing.T) {
	logger.Debug(GetAppendFile())
}

func TestGetPeers(t *testing.T) {
	logger.Debug(GetPeers())
}

func TestGetSelf(t *testing.T) {
	logger.Debug(GetSelf())
}

func TestGetDBIndex(t *testing.T) {
	logger.Debug(GetDBIndex())
}

func TestGetPassWord(t *testing.T) {
	logger.Debug(GetPassWord())
}
