package payload

import "strconv"

// IntPayload stores an int64 number
type IntPayload struct {
	Code int64
}

func NewIntPayload(code int64) *IntPayload {
	return &IntPayload{
		Code: code,
	}
}

func (r *IntPayload) Bytes() []byte {
	return []byte(":" + strconv.FormatInt(r.Code, 10) + CRLF)
}
