package payload

const (
	CRLF = "\r\n" // the line separator of redis serialization protocol
)

// PongPayload as +PONG
type PongPayload struct{}

func NewPongReplay() *PongPayload {
	return &PongPayload{}
}

var pongBytes = []byte("+PONG\r\n")

func (r *PongPayload) Bytes() []byte {
	return pongBytes
}

// OkPayload as +OK
type OkPayload struct{}

func NewOkPayload() *OkPayload {
	return &OkPayload{}
}

var okBytes = []byte("+OK\r\n")

func (r *OkPayload) Bytes() []byte {
	return okBytes
}

// NoPayload as nothing, for commands like `subscribe`
type NoPayload struct{}

func NewNoPayload() *NoPayload {
	return &NoPayload{}
}

var noBytes = []byte("")

func (r *NoPayload) Bytes() []byte {
	return noBytes
}

// QueuedPayload is +QUEUED
type QueuedPayload struct{}

func NewQueuedPayload() *QueuedPayload {
	return &QueuedPayload{}
}

var queuedBytes = []byte("+QUEUED\r\n")

func (r *QueuedPayload) Bytes() []byte {
	return queuedBytes
}
