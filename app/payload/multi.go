package payload

import (
	"bytes"
	"strconv"
)

// MultiPayload store complex list structure, for example GeoPos command
type MultiPayload struct {
	Replies []Payload
}

func NewMultiPayload(replies []Payload) *MultiPayload {
	return &MultiPayload{
		Replies: replies,
	}
}

// Bytes marshal redis.Payload
func (r *MultiPayload) Bytes() []byte {
	argLen := len(r.Replies)
	var buf bytes.Buffer
	buf.WriteString("*" + strconv.Itoa(argLen) + CRLF)
	for _, arg := range r.Replies {
		buf.Write(arg.Bytes())
	}
	return buf.Bytes()
}
