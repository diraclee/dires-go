package payload

import "strconv"

var nullBulkPayloadBytes = []byte("$-1")

// BulkPayload stores a binary-safe string
type BulkPayload struct {
	Arg []byte
}

func NewBulkPayload(arg []byte) *BulkPayload {
	return &BulkPayload{
		Arg: arg,
	}
}

func (r *BulkPayload) Bytes() []byte {
	if len(r.Arg) == 0 {
		return nullBulkPayloadBytes
	}
	return []byte("$" + strconv.Itoa(len(r.Arg)) + CRLF + string(r.Arg) + CRLF)
}

// NullBulkPayload is empty string
type NullBulkPayload struct{}

var nullBulkBytes = []byte("$-1\r\n")

func (r *NullBulkPayload) Bytes() []byte {
	return nullBulkBytes
}

func NewNullBulkPayload() *NullBulkPayload {
	return &NullBulkPayload{}
}
