package payload

// StatusPayload stores a simple status string
type StatusPayload struct {
	Status string
}

func NewStatusPayload(status string) *StatusPayload {
	return &StatusPayload{
		Status: status,
	}
}

func (r *StatusPayload) Bytes() []byte {
	return []byte("+" + r.Status + CRLF)
}
