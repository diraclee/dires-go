package payload

import (
	"bytes"
	"strconv"
)

// MultiBulkPayload stores a list of string
type MultiBulkPayload struct {
	Args [][]byte
}

func NewMultiBulkPayload(args [][]byte) *MultiBulkPayload {
	return &MultiBulkPayload{
		Args: args,
	}
}

func (r *MultiBulkPayload) Bytes() []byte {
	argLen := len(r.Args)
	var buf bytes.Buffer
	buf.WriteString("*" + strconv.Itoa(argLen) + CRLF)
	for _, arg := range r.Args {
		if arg == nil {
			buf.WriteString("$-1" + CRLF)
		} else {
			buf.WriteString("$" + strconv.Itoa(len(arg)) + CRLF + string(arg) + CRLF)
		}
	}
	return buf.Bytes()
}

// EmptyMultiBulkPayload is an empty list
type EmptyMultiBulkPayload struct{}

func NewEmptyMultiBulkPayload() *EmptyMultiBulkPayload {
	return &EmptyMultiBulkPayload{}
}

var emptyMultiBulkBytes = []byte("*0\r\n")

func (r *EmptyMultiBulkPayload) Bytes() []byte {
	return emptyMultiBulkBytes
}
