package connection

import (
	"net"
)

type channelSubscriber interface {
	Subscribe(channel string)
	UnSubscribe(channel string)
	SubsCount() int
	GetChannels() []string
	SetPassword(password string)
	GetPassword() string
}

type multiStateKeeper interface {
	InMultiState() bool
	SetMultiState(state bool)
	GetQueuedCmdLine() [][][]byte
	EnqueueCmd([][]byte)
	ClearQueuedCmds()
	GetWatching() map[string]int64
}

type dbSelector interface {
	GetDBIndex() int
	SelectDB(int)
}

type Connection interface {
	RemoteAddr() net.Addr
	Write(b []byte) (int, error)
	Close() error
	channelSubscriber //  clients keep their subscribing channels
	multiStateKeeper  // used for `Multi` command
	dbSelector        // is used for multi database
}

func New(conn net.Conn) *diresConnection {
	return &diresConnection{
		conn: conn,
	}
}
