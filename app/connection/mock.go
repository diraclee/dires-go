package connection

import "bytes"

type MockConnection struct {
	diresConnection
	buf bytes.Buffer
}


// Write writes data to buffer
func (c *MockConnection) Write(b []byte) (int, error) {
	return c.buf.Write(b)
}

// Clean resets the buffer
func (c *MockConnection) Clean() {
	c.buf.Reset()
}

// Bytes returns written data
func (c *MockConnection) Bytes() []byte {
	return c.buf.Bytes()
}
