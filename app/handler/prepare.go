package handler

import "github.com/DiracLee/dires-go/app/cmdline"

func noPrepare(args cmdline.CmdLine) ([]string, []string) {
	return nil, nil
}

func readFirstKey(args cmdline.CmdLine) ([]string, []string) {
	key := string(args[0])
	return nil, []string{key}
}

func writeFirstKey(args cmdline.CmdLine) ([]string, []string) {
	key := string(args[0])
	return []string{key}, nil
}

func readAllKeys(args cmdline.CmdLine) ([]string, []string) {
	keys := make([]string, len(args))
	for i, v := range args {
		keys[i] = string(v)
	}
	return nil, keys
}

func writeAllKeys(args cmdline.CmdLine) ([]string, []string) {
	keys := make([]string, len(args))
	for i, v := range args {
		keys[i] = string(v)
	}
	return keys, nil
}

func prepareRename(args cmdline.CmdLine) ([]string, []string) {
	src, dest := string(args[0]), string(args[1])
	return []string{src}, []string{dest}
}

func prepareRPopLPush(args cmdline.CmdLine) ([]string, []string) {
	return []string{
		string(args[0]),
		string(args[1]),
	}, nil
}

func prepareMSet(args cmdline.CmdLine) ([]string, []string) {
	size := len(args) / 2
	keys := make([]string, size)
	for i := 0; i < size; i++ {
		keys[i] = string(args[2*i])
	}
	return keys, nil
}

func prepareMGet(args cmdline.CmdLine) ([]string, []string) {
	keys := make([]string, len(args))
	for i, v := range args {
		keys[i] = string(v)
	}
	return nil, keys
}
