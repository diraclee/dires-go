package cmdline

// Management
const (
	CmdAuth         = "Auth"         // Auth password                         	| Authentication if password is needed
	CmdSubscribe    = "Subscribe"    // Subscribe channel [channel ...]       	| Subscribe given channels
	CmdPublish      = "Publish"      // Publish channel message               	| Publish a message to given channel
	CmdUnsubscribe  = "Unsubscribe"  // Unsubscribe [channel [channel ...]]   	| Unsubscribe channel(s), if no channels given, unsubscribe all
	CmdBGRewriteAOF = "BGRewriteAOF" // BGRewriteAOF -                        	| Rewrite AOF in background goroutine
	CmdRewriteAOF   = "RewriteAOF"   // RewriteAOF -                          	| Rewrite AOF
	CmdFlushAll     = "FlushAll"     // FlushAll [ASYNC|SYNC]				  	| Flush all data in current DB
	CmdSelect       = "Select"       // Select index						  	| Select DB with given index
	CmdMulti        = "Multi"        // Multi -								  	| Start Multi schema
	CmdDiscard      = "Discard"      // Discard -							  	| Discard Multi schema
	CmdExec         = "Exec"         // Exec -								  	| Exec Multi
	CmdWatch        = "Watch"        // Watch key [key ...]					  	| Watch given keys
)

// Keys
const (
	CmdKeys      = "Keys"      // Keys pattern							| Keys of given regex pattern
	CmdType      = "Type"      // Type key								| Type of given key
	CmdExists    = "Exists"    // Exists key [key ...]             	    | Judge if key(s) exist
	CmdRename    = "Rename"    // Rename key newkey                	  	| Rename given key
	CmdRenameNX  = "RenameNX"  // RenameNX key newkey                  	| Rename given key if newkey not exists
	CmdPersist   = "Persist"   // Persist key							| Set TTL of given key as unlimited
	CmdExpire    = "Expire"    // Expire key seconds					| Set TTL of given key as after given seconds
	CmdExpireAt  = "ExpireAt"  // ExpireAt key timestamp 				| Set TTL of given key as given unix timestamp seconds
	CmdPExpire   = "PExpire"   // PExpire key milliseconds				| Set TTL of given key as after given milliseconds
	CmdPExpireAt = "PExpireAt" // PExpireAt key milliseconds-timestamp	| Set TTL of given key as given unix timestamp milliseconds
	CmdTTL       = "TTL"       // TTL key								| TTL seconds of given key
	CmdPTTL      = "PTTL"      // PTTL key								| TTL milliseconds of given key
	CmdDel       = "Del"       // Del key [key ...]						| Delete given key(s)
	CmdFlushDB   = "FlushDB"   // FlushDB [ASYNC]						| Flush DB
)

// String
const (
	CmdSet         = "Set"         // Set key value [expiration EX seconds|PX milliseconds] [NX|XX]		| Set key with given value. EX specify TTL in second, PX specify TTL in millisecond; NX set insert policy (PutIfNotExists), XX set update policy (SetIfExists), default is upsert policy (PutOrSet)
	CmdSetNX       = "SetNX"       // SetNX key value													| Set key with given value if given key does not exist
	CmdSetEX       = "SetEX"       // SetEX key seconds value											| Set key with given value and given TTL seconds
	CmdPSetEX      = "PSetEX"      // PSetEX key milliseconds value										| Set key with given value and given TTL milliseconds
	CmdMSet        = "MSet"        // MSet key value [key value ...]									| Multi set key-value pairs
	CmdMSetNX      = "MSetNX"      // MSetNX key value [key value ...]									| Multi set key-value pairs if given key does not exist
	CmdGet         = "Get"         // Get key															| Get value for given key
	CmdMGet        = "MGet"        // MGet key [key ...]												| Get values for given keys
	CmdGetSet      = "GetSet"      // GetSet key value													| Set value and return old value for given key
	CmdIncr        = "Incr"        // Incr key															| Increase value by 1 for given key
	CmdIncrBy      = "IncrBy"      // IncrBy key increment												| Increase value by given integer increment for given key
	CmdIncrByFloat = "IncrByFloat" // IncrByFloat key increment											| Increase value by given decimal increment for given key
	CmdDecr        = "Decr"        // Decr key															| Decrease value by 1 for given key
	CmdDecrBy      = "DecrBy"      // DecrBy key decrement												| Decrease value by given integer increment for given key
	CmdStrLen      = "StrLen"      // StrLen key														| String length of value for given key
	CmdAppend      = "Append"      // Append key value													| Append as suffix with given value to value for given key
	CmdGetRange    = "GetRange"    // GetRange key start end											| Get substring between given start and end of value for given key
	CmdSetRange    = "SetRange"    // SetRange key offset value											| Set substring from given offset of value for given key
)

// List
const (
	CmdLPush     = "LPush"     // LPush key value [value ...]			| Push left given value into list for given key
	CmdLPushX    = "LPushX"    // LPushX key value						| Push left given value into list for given key if the list exits
	CmdRPush     = "RPush"     // RPush key value [value ...]			| Push right given value into list for given key
	CmdRPushX    = "RPushX"    // RPushX key value						| Push right given value into list for given key if the list exits
	CmdLPop      = "LPop"      // LPop key								| Pop and return left elem of list for given key
	CmdRPop      = "RPop"      // RPop key								| Pop and return right elem of list for given key
	CmdRPopLPush = "RPopLPush" // RPopLPush source destination			| RPop from source list and LPush to destination list
	CmdLRem      = "LRem"      // LRem key count value					| Remove given amount of given value
	CmdLLen      = "LLen"      // LLen key								| Length of list for given key
	CmdLIndex    = "LIndex"    // LIndex key index						| Element of given index
	CmdLSet      = "LSet"      // LSet key index value					| Set element of given index as given value
	CmdLRange    = "LRange"    // LRange key start stop					| Elements between start and stop index
)

// Hash
const (
	CmdHSet         = "HSet"         // HSet key field value						| Set given field as given value pair for given key
	CmdHSetNX       = "HSetNX"       // HSetNX key field value						| Set given field as given value pair for given key if filed does not exist
	CmdHMSet        = "HMSet"        // HMSet key field value [field value ...]		| Set field-value pairs for given key
	CmdHGet         = "HGet"         // HGet key field								| Get value of given field for given key
	CmdHMGet        = "HMGet"        // HMGet key field [field ...]					| Get values of given fields for given key
	CmdHExists      = "HExists"      // HExists key field							| Judge if given field exists for given key
	CmdHLen         = "HLen"         // HLen key	 								| Fields count for given key
	CmdHDel         = "HDel"         // HDel key field [field ...]					| Delete given fields for given key
	CmdHKeys        = "HKeys"        // HKeys key									| All fields of given key
	CmdHVals        = "HVals"        // HVals key									| All values of given key
	CmdHGetAll      = "HGetAll"      // HGetAll key									| All field-value pairs
	CmdHIncrBy      = "HIncrBy"      // HIncrBy key field increment					| Increase value by given integer increment of given field for given key
	CmdHIncrByFloat = "HIncrByFloat" // HIncrByFloat key field increment			| Increase value by given decimal increment of given field for given key
)

// Set
const (
	CmdSAdd        = "SAdd"        // SAdd key member [member ...]				|
	CmdSIsMember   = "SIsMember"   // SIsMember key member						|
	CmdSRem        = "SRem"        // SRem key member [member ...]				|
	CmdSCard       = "SCard"       // SCard key									|
	CmdSMembers    = "SMembers"    // SMembers key								|
	CmdSInter      = "SInter"      // SInter key [key ...]						|
	CmdSInterStore = "SInterStore" // SInterStore destination key [key ...]		|
	CmdSUnion      = "SUnion"      // SUnion key [key ...]						|
	CmdSUnionStore = "SUnionStore" // SUnionStore destination key [key ...]		|
	CmdSDiff       = "SDiff"       // SDiff key [key ...]						|
	CmdSDiffStore  = "SDiffStore"  // SDiffStore destination key [key ...]		|
	CmdSRandMember = "SRandMember" // SRandMember key [count]					|
)

// ZSet
const (
	CmdZAdd             = "ZAdd"             // ZAdd key [NX|XX] [CH] [INCR] score member [score member ...]		|
	CmdZScore           = "ZScore"           // ZScore key member													|
	CmdZIncrBy          = "ZIncrBy"          // ZIncrBy key increment member										|
	CmdZRank            = "ZRank"            // ZRank key member													|
	CmdZCount           = "ZCount"           // ZCount key min max													|
	CmdZRevRank         = "ZRevRank"         // ZRevRank key member													|
	CmdZCard            = "ZCard"            // ZCard key															|
	CmdZRange           = "ZRange"           // ZRange key start stop [WITHSCORES]									|
	CmdZRangeByScore    = "ZRangeByScore"    // ZRangeByScore key min max [WITHSCORES] [LIMIT offset count]			|
	CmdZRevRange        = "ZRevRange"        // ZRevRange key start stop [WITHSCORES]								|
	CmdZRevRangeByScore = "ZRevRangeByScore" // ZRevRangeByScore key max min [WITHSCORES] [LIMIT offset count]		|
	CmdZRem             = "ZRem"             // ZRem key member [member ...]										|
	CmdZRemRangeByScore = "ZRemRangeByScore" // ZRemRangeByScore key min max										|
	CmdZRemRangeByRank  = "ZRemRangeByRank"  // ZRemRangeByRank key start stop										|
)

// Geo
const (
	CmdGeoAdd            = "GeoAdd"            // GeoAdd key longitude latitude member [longitude latitude member ...]																			|
	CmdGeoPos            = "GeoPos"            // GeoPos key member [member ...]																												|
	CmdGeoDist           = "GeoDist"           // GeoDist key member1 member2 [unit]																											|
	CmdGeoHash           = "GeoHash"           // GeoHash key member [member ...]																												|
	CmdGeoRadius         = "GeoRadius"         // GeoRadius key longitude latitude radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [COUNT count] [ASC|DESC] [STORE key] [STOREDIST key]		|
	CmdGeoRadiusByMember = "GeoRadiusByMember" // GeoRadiusByMember key member radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [COUNT count] [ASC|DESC] [STORE key] [STOREDIST key]			|
)
