package cmdline

import "strings"

type CmdLine [][]byte

func (cmd CmdLine) String() string {
	var b strings.Builder
	b.Grow(len(cmd))
	b.Write(cmd[0])
	for _, bytes := range cmd[1:] {
		b.WriteString(" ")
		b.Write(bytes)
	}
	return b.String()
}
