module github.com/DiracLee/dires-go

go 1.16

require (
	github.com/fatih/color v1.13.0
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0 // indirect
)
