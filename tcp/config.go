package tcp

import (
	"time"
)

type Config struct {
	Address    string
	MaxConnect int64
	Timeout    time.Duration
}

func NewConfig(options ...Option) *Config {
	config := &Config{
		Address:    "127.0.0.1:8000",
		MaxConnect: 10,
		Timeout:    10,
	}
	for _, option := range options {
		option.Apply(config)
	}
	return config
}

type Option interface {
	Apply(config *Config)
}

type OptionFunc func(config *Config)

func (f OptionFunc) Apply(config *Config) {
	f(config)
}

func WithAddress(address string) Option {
	return OptionFunc(func(config *Config) {
		if address == "" {
			return
		}
		config.Address = address
	})
}

func WithMaxConnect(maxConnect int64) Option {
	return OptionFunc(func(config *Config) {
		if maxConnect == 0 {
			return
		}
		config.MaxConnect = maxConnect
	})
}

func WithTimeout(timeout time.Duration) Option {
	return OptionFunc(func(config *Config) {
		if timeout == 0 {
			return
		}
		config.Timeout = timeout
	})
}
