package handler

import (
	"context"
	"github.com/DiracLee/dires-go/app/config"
	"github.com/DiracLee/dires-go/app/server/cluster"
	"github.com/DiracLee/dires-go/app/server/standalone"
	"net"
)

type Handler interface {
	Handle(ctx context.Context, conn net.Conn)
	Close() error
}

func NewDiresHandler() *DiresHandler {
	svr := standalone.NewStandalone()
	if config.IsClusterNode() {
		svr = cluster.NewClusterNode()
	}
	return &DiresHandler{
		server: svr,
	}
}

func NewEchoHandler() *EchoHandler {
	return &EchoHandler{}
}

type HandleFunc func(ctx context.Context, conn net.Conn)

func (f HandleFunc) Handle(ctx context.Context, conn net.Conn) {
	f(ctx, conn)
}

func (f HandleFunc) Close() error {
	return nil
}
