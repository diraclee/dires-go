package utils

import "math/rand"

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// RandString create a random string no longer than n
func RandString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}


func BytesSliceToStrings(bytesSlice [][]byte) []string {
	strs := make([]string, 0, len(bytesSlice))
	for _, bytes := range bytesSlice {
		strs = append(strs, string(bytes))
	}
	return strs
}