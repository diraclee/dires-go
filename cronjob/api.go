package cronjob

import "time"

var cj *cronjob

func init() {
	cj = New()
	cj.Start()
}

func RegisterDelay(delay time.Duration, taskID string, job func() error) {
	cj.RegisterDelay(delay, taskID, job)
}

func RegisterTimed(t time.Time, taskID string, job func() error) {
	cj.RegisterDelay(t.Sub(time.Now()), taskID, job)
}

func Cancel(taskID string) {
	cj.Cancel(taskID)
}
