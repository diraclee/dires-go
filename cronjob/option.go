package cronjob

import (
	"github.com/DiracLee/dires-go/ds/dict"
	"time"
)

type Option interface {
	Apply(*cronjob)
}

type OptionFunc func(*cronjob)

func (f OptionFunc) Apply(cj *cronjob) {
	f(cj)
}

func WithInterval(interval time.Duration) Option {
	return OptionFunc(func(cj *cronjob) {
		cj.interval = interval
		cj.ticker = time.NewTicker(interval)
	})
}

func WithSlotCount(slotCount int) Option {
	return OptionFunc(func(cj *cronjob) {
		cj.taskSlots = make([]slot, slotCount)
		for i := 0; i < len(cj.taskSlots); i++ {
			cj.taskSlots[i] = dict.NewConcurrent(16)
		}
	})
}

func WithChAddBuffSize(chAddBuffSize int) Option {
	return OptionFunc(func(cj *cronjob) {
		cj.chAdd = make(chan task, chAddBuffSize)
	})
}

func WithChRemoveBuffSize(chRemoveBuffSize int) Option {
	return OptionFunc(func(cj *cronjob) {
		cj.chRemove = make(chan string, chRemoveBuffSize)
	})
}
