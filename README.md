# Dires

An implementation of redis server in Golang.

## Supported Commands

### Management

#### Auth

```
Auth password 
```

Authentication if password is needed

#### Subscribe

```
Subscribe channel [channel ...]        
```

Subscribe given channels

#### Publish

```
Publish channel message 
```

Publish a message to given channel

#### Unsubscribe

```
Unsubscribe [channel [channel ...]]    
```

Unsubscribe channel(s), if no channels given, unsubscribe all

#### BGRewriteAOF

```
BGRewriteAOF - 
```

Rewrite AOF in background goroutine

#### RewriteAOF

```
RewriteAOF - 
```

Rewrite AOF

#### FlushAll

```
FlushAll [ASYNC|SYNC]                    
```

Flush all data in current DB

#### Select

```
Select index 
```

Select DB with given index

#### Multi

```
Multi - 
```

Start Multi schema

#### Discard

```
Discard - 
```

Discard Multi schema

#### Exec

```
Exec - 
```

Exec Multi

#### Watch

```
Watch key [key ...]                        
```

Watch given keys

### Keys

#### Keys

```
Keys pattern
```

Keys of given regex pattern

#### Type

```
Type key
```

Type of given key

#### Exists

```
Exists key [key ...]                 
```

Judge if key(s) exist

#### Rename

```
Rename key newkey                  
```

Rename given key

#### RenameNX

```
RenameNX key newkey                  
```

Rename given key if newkey not exists

#### Persist

```
Persist key
```

Set TTL of given key as unlimited

#### Expire

```
Expire key seconds
```

Set TTL of given key as after given seconds

#### ExpireAt

```
ExpireAt key timestamp 
```

Set TTL of given key as given unix timestamp seconds

#### PExpire

```
PExpire key milliseconds
```

Set TTL of given key as after given milliseconds

#### PExpireAt

```
PExpireAt key milliseconds-timestamp
```

Set TTL of given key as given unix timestamp milliseconds

#### TTL

```
TTL key
```

TTL seconds of given key

#### PTTL

```
PTTL key
```

TTL milliseconds of given key

#### Del

```
Del key [key ...]
```

Delete given key(s)

#### FlushDB

```
FlushDB [ASYNC]
```

Flush DB

### String

#### Set

```
Set key value [expiration EX seconds|PX milliseconds] [NX|XX]        
```

Set key with given value.

- EX specify TTL in second
- PX specify TTL in millisecond
- NX set insert policy (PutIfNotExists)
- XX set update policy (SetIfExists), default is upsert policy (PutOrSet)

#### SetNX

```
SetNX key value 
```

Set key with given value if given key does not exist

#### SetEX

```
SetEX key seconds value 
```

Set key with given value and given TTL seconds

#### PSetEX

```
PSetEX key milliseconds value 
```

Set key with given value and given TTL milliseconds

#### MSet

```
MSet key value [key value ...]                                    
```

Multi set key-value pairs

#### MSetNX

```
MSetNX key value [key value ...]                                    
```

Multi set key-value pairs if given key does not exist

#### Get

```
Get key 
```

Get value for given key

#### MGet

```
MGet key [key ...]                                                
```

Get values for given keys

#### GetSet

```
GetSet key value 
```

Set value and return old value for given key

#### Incr

```
Incr key 
```

Increase value by 1 for given key

#### IncrBy

```
IncrBy key increment 
```

Increase value by given integer increment for given key

#### IncrByFloat

```
IncrByFloat key increment 
```

Increase value by given decimal increment for given key

#### Decr

```
Decr key 
```

Decrease value by 1 for given key

#### DecrBy

```
DecrBy key decrement 
```

Decrease value by given integer increment for given key

#### StrLen

```
StrLen key 
```

String length of value for given key

#### Append

```
Append key value 
```

Append as suffix with given value to value for given key

#### GetRange

```
GetRange key start end 
```

Get substring between given start and end of value for given key

#### SetRange

```
SetRange key offset value 
```

Set substring from given offset of value for given key

### List

#### LPush

```
LPush key value [value ...]
```

Push left given value into list for given key

#### LPushX

```
LPushX key value
```

Push left given value into list for given key if the list exits

#### RPush

```
RPush key value [value ...]
```

Push right given value into list for given key

#### RPushX

```
RPushX key value
```

Push right given value into list for given key if the list exits

#### LPop

```
LPop key
```

Pop and return left elem of list for given key

#### RPop

```
RPop key
```

Pop and return right elem of list for given key

#### RPopLPush

```
RPopLPush source destination
```

RPop from source list and LPush to destination list

#### LRem

```
LRem key count value
```

Remove given amount of given value

#### LLen

```
LLen key
```

Length of list for given key

#### LIndex

```
LIndex key index
```

Element of given index

#### LSet

```
LSet key index value
```

Set element of given index as given value

#### LRange

```
LRange key start stop
```

Elements between start and stop index

### Hash

#### HSet

```
HSet key field value
```

Set given field as given value pair for given key

#### HSetNX

```
HSetNX key field value
```

Set given field as given value pair for given key if filed does not exist

#### HMSet

```
HMSet key field value [field value ...]
```

Set field-value pairs for given key

#### HGet

```
HGet key field
```

Get value of given field for given key

#### HMGet

```
HMGet key field [field ...]
```

Get values of given fields for given key

#### HExists

```
HExists key field
```

Judge if given field exists for given key

#### HLen

```
HLen key 
```

Fields count for given key

#### HDel

```
HDel key field [field ...]
```

Delete given fields for given key

#### HKeys

```
HKeys key
```

All fields of given key

#### HVals

```
HVals key
```

All values of given key

#### HGetAll

```
HGetAll key
```

All field-value pairs

#### HIncrBy

```
HIncrBy key field increment
```

Increase value by given integer increment of given field for given key

#### HIncrByFloat

```
HIncrByFloat key field increment
```

Increase value by given decimal increment of given field for given key


